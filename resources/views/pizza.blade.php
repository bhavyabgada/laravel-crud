@extends('layouts.layout')
@section('content')
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
    <div class="top-right links">
        @auth
        <a href="{{ url('/home') }}">Home</a>
        @else
        <a href="{{ route('login') }}">Login</a>

        @if (Route::has('register'))
        <a href="{{ route('register') }}">Register</a>
        @endif
        @endauth
    </div>
    @endif



    <div class="content">
        <div class="title m-b-md">
            Pizza
        </div>

        <p>
            {{ $type }} - {{ $base }} - {{ $price }}
        </p>

        @if( $price < 10)
            <p>This Pizza is Cheap.</p>
        @elseif( $price > 20)
            <p>This Pizza is Expensive.</p>
        @else
            <p>This Pizza is Normally Priced.</p>
        @endif

        @unless( $base == 'Cheezy Crust' )
            <p>You Dont have a 'Cheezy Crust'.</p>
        @endunless



    </div>
</div>
@endsection
